#ifndef MONPITOR_VCHIGENCMD_H
#define MONPITOR_VCHIGENCMD_H

extern "C" {
#include <interface/vmcs_host/vc_vchi_gencmd.h>
}

#include <string>
#include <vector>
#include <map>
class VchiService;

class VchiGenCmd {
public:
  VchiGenCmd(VchiService& service);
	~VchiGenCmd();

  template<class ...Args>
	std::map<std::string, std::string> command(const std::string& cmd, Args&&... args);
	std::map<std::string, std::string> command(const std::string& cmd);

	static std::vector<std::string> parseThrottled(int flags);
	static inline std::vector<std::string> parseThrottled(const std::string& flags) { return parseThrottled(std::stoi(flags, 0, 0)); }

private:
	VchiService* service;
	VCHI_CONNECTION_T* conn;
};

#endif
