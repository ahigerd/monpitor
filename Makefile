VC_INCLUDES = /opt/vc/include
VC_LIB_PATH = /opt/vc/lib

monpitor: $(patsubst %.cpp, %.o, $(wildcard *.cpp))
	g++ -o $@ $^ -L$(VC_LIB_PATH) -lpthread -lvcos -lvchiq_arm -lvchostif

monpitor.o: vchiservice.h vchigencmd.h vcerror.h commandargs.h
vcerror.o: vcerror.h
vchigencmd.o: vchigencmd.h  vchiservice.h vcerror.h
vchiservice.o: vchiservice.h vcerror.h

%.o: %.h

%.o: %.cpp
	g++ -o $@ -O2 -c $< -std=gnu++11 -I$(VC_INCLUDES) 

clean:
	rm -f monpitor *.o
