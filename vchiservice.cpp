#include "vchiservice.h"
#include "vcerror.h"

VchiService::VchiService() : instance(nullptr) {
  vcos_init();
	int32_t err = vchi_initialise(&instance);
	if (err) {
	  throw VcError(err, "error initializing VCHI"); 
	}

	err = vchi_connect(nullptr, 0, instance);
	if (err) {
	  throw VcError(err, "error connecting to VCHI service"); 
	}
}

VchiService::~VchiService() {
  if (instance) {
	  vchi_disconnect(instance);
	}
}

VchiService::operator VCHI_INSTANCE_T() const {
  return instance;
}
