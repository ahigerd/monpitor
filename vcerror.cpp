#include "vcerror.h"

VcError::VcError(int code, const std::string& msg) : std::runtime_error(msg), _code(code) {
  // initializers only
}

VcError::VcError(const std::string& code, const std::string& msg) : std::runtime_error(msg), _code(std::stoi(code)) {
  // initializers only
}

int VcError::code() const noexcept {
  return _code;
}
