#ifndef MONPITOR_VCHISERVICE_H
#define MONPITOR_VCHISERVICE_H

extern "C" {
#include <interface/vchi/vchi.h>
}

class VchiService {
public:
  VchiService();
	~VchiService();

	operator VCHI_INSTANCE_T() const;

private:
  VCHI_INSTANCE_T instance;
};

#endif
