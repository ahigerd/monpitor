#include "vchigencmd.h"
#include "vchiservice.h"
#include "vcerror.h"
#include <sstream>

extern "C" {
#include <interface/vmcs_host/vc_gencmd_defs.h>
}

const std::map<int, std::string> throttledMessages = {
  { 0, "undervolted" },
	{ 1, "CPU frequency capped" },
	{ 2, "throttled" },
	{ 3, "soft temperature limit active" },
	{ 16, "undervolt event observed" },
	{ 17, "CPU frequency cap event observed" },
	{ 18, "throttle event observed" },
	{ 19, "soft temperature limit activation observed" },
};

std::vector<std::string> VchiGenCmd::parseThrottled(int flags) {
	std::vector<std::string> result;

  int i = 0;
	while (flags) {
	  if ((flags & 0x1) && throttledMessages.count(i)) {
		  result.push_back(throttledMessages.at(i));
		}
		flags >>= 1;
		i += 1;
	}

	return result;
}

static inline void buildCommand(std::stringstream& ss) {
  // no-op
}

template<class T, class ...Args>
static inline void buildCommand(std::stringstream& ss, T&& head, Args&&... rest) {
  ss << " " << head;
	buildCommand(ss, std::forward(rest)...);
}

static inline std::string buildCommand(const std::string& command) {
	return command;
}

template<class ...Args>
static inline std::string buildCommand(const std::string& command, Args&&... args) {
  std::stringstream ss;
	ss << command;
	buildCommand(ss, std::forward(args)...);
	return ss.str();
}


VchiGenCmd::VchiGenCmd(VchiService& _service) : service(&_service), conn(nullptr) {
  vc_vchi_gencmd_init(_service, &conn, 1);
}

VchiGenCmd::~VchiGenCmd() {
  if (conn) {
		vc_gencmd_stop();
	}
}

template<class ...Args>
std::map<std::string, std::string> VchiGenCmd::command(const std::string& cmd, Args&&... args) {
  return command(buildCommand(cmd, std::forward(args)...));
}

std::map<std::string, std::string> VchiGenCmd::command(const std::string& cmd) {
	std::string buffer(GENCMDSERVICE_MSGFIFO_SIZE, '\0');

	int32_t err = vc_gencmd_send(cmd.c_str());
	if (err) {
	  throw VcError(err, "error invoking gencmd");
	}

	err = vc_gencmd_read_response(const_cast<char*>(buffer.data()), buffer.size());
	if (err) {
	  throw VcError(err, "error reading gencmd response");
	}

	std::map<std::string, std::string> result;

  int pos = 0;
	int tokenStart = 0;
	char ch;
	std::string key;
	int state = 0;
	while (pos < buffer.length() && (ch = buffer[pos]) != '\0') {
	  if (state == 0 && ch == '=') {
		  key = buffer.substr(tokenStart, pos - tokenStart);
			tokenStart = pos + 1;
			state = 1;
		} else if (state == 1 && ch == '"') {
			tokenStart = pos + 1;
		  state = 2;
	  } else if (state == 1 && (ch == ' ' || ch == '\n')) {
		  result[key] = std::string();
			tokenStart = pos + 1;
			state = 0;
		} else if (state == 1) {
		  state = 3;
		} else if ((state == 2 && ch == '"') || (state == 3 && (ch == ' ' || ch == '\n'))) {
		  result[key] = buffer.substr(tokenStart, pos - tokenStart);
			tokenStart = pos + 1;
			state = 0;
		}
		pos++;
	}
	if (state > 0) {
		result[key] = buffer.substr(tokenStart, pos - tokenStart);
	}

	if (result.count("error")) {
	  throw VcError(result["error"], result["error_msg"]);
	}

  return result;
}
