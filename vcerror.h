#ifndef MONPITOR_VCERROR_H
#define MONPITOR_VCERROR_H

#include <stdexcept>

class VcError : public std::runtime_error {
public:
  VcError(int code, const std::string& msg);
  VcError(const std::string& code, const std::string& msg);

	virtual int code() const noexcept;

private:
  int _code;
};

#endif
