#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include <chrono>
#include <thread>
#include <ctime>
#include "vchiservice.h"
#include "vchigencmd.h"
#include "vcerror.h"
#include "commandargs.h"

#define CPU_TEMP_PATH "/sys/class/thermal/thermal_zone0/temp"

static float getCpuTemp() {
  std::fstream cpuFile(CPU_TEMP_PATH, std::ios::in);
  std::string cpuTemp;
  std::getline(cpuFile, cpuTemp);
  return std::stof(cpuTemp) / 1000.0;
}

static void explainThrottled(int flags) {
  for (const auto& flag : VchiGenCmd::parseThrottled(flags)) {
    std::cout << "  " << flag << std::endl;
  }
}

int main(int argc, char** argv) {
  CommandArgs args({
    { "help", "h", "", "Show this help text" },
    { "count", "c", "times", "Stop after outputting this many samples (default: unlimited)" },
    { "interval", "n", "seconds", "Sample every n seconds (default: 2.0)" },
    { "run", "r", "command", "Sends a command to the VideoCore and displays the response" },
    { "explain", "x", "flags", "Explains a value returned by get_throttled" },
    { "", "", "format", "Selects and orders the displayed fields" },
  });
  args.parse(argc, argv);

  if (args.hasKey("help")) {
    std::cout << args.usageText(argv[0]) << std::endl;
    return 0;
  }
  if (args.hasKey("explain")) {
    explainThrottled(args.getInt("explain"));
    return 0;
  }

  VchiService vchi;
  VchiGenCmd gencmd(vchi);
  std::map<std::string, std::string> result;

  if (args.hasKey("run")) {
    try {
      result = gencmd.command(args.getString("run"));
    } catch (VcError& err) {
      std::cerr << "Error " << err.code() << ": " << err.what() << std::endl;
      return -1;
    }
    for (auto kv : result) {
      std::cout << kv.first << "=\"" << kv.second << '"' << std::endl;
    }
    if (result.count("throttled")) {
      explainThrottled(std::stoi(result["throttled"], 0, 0));
    }
    return 0;
  }

  int64_t sleepInterval = args.getFloat("interval", 2.0) / 1000.0;
  int maxCount = args.getInt("count", -1);
  if (sleepInterval <= 0) {
    std::cerr << argv[0] << ": invalid sleep interval";
    return -1;
  }
  if (maxCount == 0) {
    std::cerr << argv[0] << ": invalid sample count";
    return -1;
  }

  while (true) {
    int throttleStatus = std::stoi(gencmd.command("get_throttled")["throttled"], 0, 0);
    result = gencmd.command("measure_temp");
    std::time_t now = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
    std::cout << std::showpoint 
      << std::string(std::ctime(&now)).substr(0, 24)
      << " | CPU: " << std::setw(4) << std::setprecision(3) << getCpuTemp() << "C"
      << " | GPU: " << std::setw(4) << std::setprecision(3) << std::stof(result["temp"]) << "C";
    if (!(throttleStatus & 0xF)) {
      std::cout << " | OK";
    } else {
      if (throttleStatus & 0x01) std::cout << " | undervolt";
      if (throttleStatus & 0x02) std::cout << " | freq cap";
      if (throttleStatus & 0x04) std::cout << " | throttled";
      if (throttleStatus & 0x08) std::cout << " | overheat";
    }
    std::cout << std::endl;

    if (maxCount > 0) {
      --maxCount;
      if (maxCount <= 0) return 0;
    }
    std::this_thread::sleep_for(std::chrono::milliseconds(sleepInterval));
  }

  return 0;
}
